<!DOCTYPE HTML>
<html>
<head>

<meta charset="utf-8">
<title>Fred Julienne: web & maintenance informatique</title>

<!-- Meta balises  -->
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="content-language" content="fr">
<meta http-equiv="imagetoolbar" content="no">
<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.2)">
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.2)">
<meta http-equiv="Content-Style-Type" content="text/css">

<meta name="Keywords" content="julienne, frédéric, technicien, informatique, web, linux, web, internet, php, html, javascript, hérault, musique, infographie, graphisme, design, multimédia, réseau, intranet, serveur, gestion, logo, maintenance, infographie, net, sql, unix, windows, word, excel, access, office, powerpoint, adobe, photoshop, indesign, illustrator, steinberg, cubase, 3d, php, jquery, fullpage">
<meta name="description" content="Bienvenue sur le site de Frédéric JULIENNE. ">
<meta name="author" content="Frédéric JULIENNE">
<meta name="copyright" content="Copyright &copy; 2014 / Frédéric JULIENNE.">
<meta name="identifier-url" content="http://fredjulienne.free.fr">

<meta name="robots" content="all">
<meta name="revisit-after" content="30 days">

<link rel="stylesheet" href="css.css">
<link rel="stylesheet" type="text/css" href="customs.css" />
<link rel="stylesheet" type="text/css" href="fullPage.js-master/jquery.fullPage.css" />

<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

<link rel="apple-touch-icon" href="apple-touch-icon.png" />
<link rel="icon" type="image/png" href="favicon.png" />

<link href='http://fonts.googleapis.com/css?family=Gloria+Hallelujah' rel='stylesheet'>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>		
	
<script src="fullPage.js-master/jquery.fullPage.js"></script>

</head>

<body>
        <video id="video_background" autoplay loop>
            <source src="img/video.mp4" type="video/mp4">
            <source src="img/video.webm" type="video/webm">
            <source src="img/video.ogv" type="video/ogv">
            <source src="img/video.ogg" type="video/ogg"> 
            Vidéo non supportée
        </video>

<div id="fullpage">

    	
    <div class="section" id="section0">
        <div class="intro">
			
			<h1>Bienvenue sur le site de Frédéric JULIENNE</h1>
            <img src="img/titre.svg"  alt="Bienvenu"/>
			<p>Que peut-il faire pour vous ?<br/></p><br>
            <audio id="player" controls loop>
                <source src="sound/son.mp3" type="audio/mpeg">
                <source src="sound/son.ogg" type="audio/ogg">
                Votre navigateur ne supporte pas ce format audio.
			</audio><br>
            <h2>Faites défiler vers le bas avec la molette de votre souris ou utilisez la flèches vers le bas  de votre clavier pour commencer.</h2>
      
        </div>
    </div>
	
  <div class="section" id="section1">
	    <div class="slide" id="maintenance" data-anchor="maintenance">
            <div class="intro">
      
                <h1>Maintenance Informatique</h1>
                <p>Il sait diagnostiquer les pannes matérielles et logicielles de parc informatique. Son expérience dans des collectivités locales et entreprises lui ont permis d'appréhender la plupart des pannes. Intervenir sur des serveurs, régler des conflits IP, mise à jour de parc informatique en adéquation aux spécificités des logiciels utilisés, trouver des solutions matérielles qui répondent aux problématiques des clients : tout ça il sait faire !</p>
        <img src="img/etoile1.svg"  alt="etoile1"/>
			
            </div>
		</div>
		
	    <div class="slide" id="infographie" data-anchor="infographie">
			
            <h1>Infographie</h1>
            <p>Avec son expérience dans la PAO et le Web, Frédéric est capable de proposer des maquettes dont les formats ne soient pas incompatibles avec les services d'impression. Il est capable d'adapter la plupart des média numériques (images, vidéos ou son) à des fins d'exploitation optimales.
Et bien sûr, avec la convergence des technologies orienté web, il sait être à l'écoute des nouvelles technologies.</p>
            <img src="img/etoile2.svg"  alt="etoile2"/>
        
        </div>
		
        <div class="slide" id="web" data-anchor="web">
			<h1>Web</h1>
            <p>Webdesigner ? Webmaster ? Intégrateur html etc. Tous ces domaines ne doivent être négligés pour la conception d'un site web. De ce fait, il prend en compte tous les aspects importants (design, interface, fonctionnalités mais aussi hébergement, référencement, compatibilité des navigateurs, des médias…). Il est donc sensibilisé aux technologies actuelles (architecture n-tiers, design responsive, utilisation de framework orienté MVC, CMS, jquery) et jongle entre le JavaScript et le PHP.</p>
            <img src="img/etoile3.svg"  alt="etoile3"/>
        
        </div>	
    
    	<div class="slide" id="pff" data-anchor="pff">
			<h1>Pff…</h1>
            <p>Cela ne dit pas grand-chose tout ça ! Et ce site: pourquoi ?<br/>Vous avez devant-vous un site « tendance » :<br/> 
C'est un site one-page (sur une seule page pour plus de fluidité) fait en html5/CSS3 et jquery. Toutes les images sont vectorielles (svg). Ce site s'adapte à tous médias susceptibles de pouvoir afficher une page web.<br>Pas mal non ?</p>
            <img src="img/etoile4.svg"  alt="etoile4"/>
        
        </div>
    
    	<div class="slide" id="contact" data-anchor="contact">
            <h1>Intéressé?</h1><br/>
        <p>contactez-moi par  courriel à <br/>
        <a href="mailto:cyberfredrx@hotmail.com">cyberfredrx@hotmail.com</a></p>
        
        </div>

	</div>
    
</div>
<footer>
<script type="text/javascript">
	$(document).ready(function() {
		$('#fullpage').fullpage({
			anchors: ['bienvenue', 'visitez','contact'],
			slidesNavigation: true,
            css3: true,
            afterRender: function(){
					//setting the size
					$('video').height($(window).height());
					//playing the video
					$('video').get(0).play();
				}
            
		});
	});
    
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51103279-1', 'free.fr');
  ga('send', 'pageview');

    
</script>
</footer>
</body>
</html>