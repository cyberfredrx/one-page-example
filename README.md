# FredRX
Site Web de J. Fred, implementé en [PHP](https://php.net).

## Technologies
Ce projet a été développé principalement avec ces bibliothèques :

#### Application client
* [jQuery](https://jquery.com)
* [fullPage](https://github.com/alvarotrigo/fullPage.js)

## License
[FredRX](http://fredjulienne.free.fr) est distribué sous une licence propriétaire.

## URL
Please visit [http://fredjulienne.free.fr](http://fredjulienne.free.fr) for details.